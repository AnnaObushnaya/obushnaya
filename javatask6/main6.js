// Метод forEach  используется, когда нужно по одному перебрать все элементы
// массива.

function filterBy(input, typeofArgument) {
    let result = new Array();
    input.forEach(function (item, i, arr) {
        if (typeof item !== typeofArgument ) {
            result.push(item);
        }
    });
    return result;
}
let arr = ['hello', 'world', 23, '23', null];
let result = filterBy(arr, 'string');
console.log(result);
